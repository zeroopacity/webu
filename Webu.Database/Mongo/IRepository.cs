namespace Webu.Database.Mongo{
    public interface IRepository{
        T Get<T>(int id);
    }
}