namespace Webu.Database.Mongo
{
    public abstract class BaseRepository
    {
        public DbContext Ctx;
        public BaseRepository()
        {
           Ctx = new DbContext();
        }
    }
}