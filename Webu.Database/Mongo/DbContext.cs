using MongoDB.Driver;
using Webu.Entity;
using Webu.Core;


namespace Webu.Database.Mongo
{
    public class DbContext
    {
        IMongoDatabase _db;
        public DbContext()
        {
            MongoClient _client = new MongoClient(Application.Config["MongoDbURL"]);
            _db = _client.GetDatabase("Webu");
        }

        IMongoCollection<User> _users;
        public IMongoCollection<User> Users
        {
            get
            {
                _users = _users ?? _db.GetCollection<User>("User");
                return _users;
            }
        }
    }
}