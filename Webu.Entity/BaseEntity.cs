namespace Webu.Entity
{
    public class BaseEntity
    {
        [MongoDB.Bson.Serialization.Attributes.BsonId]
        public MongoDB.Bson.ObjectId _id { get; set; }
    }
}