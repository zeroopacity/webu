using Microsoft.AspNetCore.Http;
using System.Text;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using System;

namespace Webu.Core
{
    public static class Extension
    {
        /**
       Set value in session */
        public static void Set<T>(this ISession session, string key, T value)
        {
            var json = JsonConvert.SerializeObject(value);
            var jsonBytes = Encoding.UTF8.GetBytes(json);
            session.Set(key, jsonBytes);
        }
        /**
        Get Session value from store
         */
        public static T Get<T>(this ISession session, string key)
        {
            var valueFromRedis = default(byte[]);
            session.TryGetValue(key, out valueFromRedis);
            if (valueFromRedis == null)
            {
                return default(T);
            }
            var strValue = Encoding.UTF8.GetString(valueFromRedis);
            return JsonConvert.DeserializeObject<T>(strValue);
        }

        public static string QueryString(this HttpRequest request, string key)
        {
            StringValues str;
            request.Query.TryGetValue(key, out str);
            return str;
        }

        /// <summary>
        /// Determines whether the specified HTTP request is an AJAX request.
        /// </summary>
        /// 
        /// <returns>
        /// true if the specified HTTP request is an AJAX request; otherwise, false.
        /// </returns>
        /// <param name="request">The HTTP request.</param><exception cref="T:System.ArgumentNullException">The <paramref name="request"/> parameter is null (Nothing in Visual Basic).</exception>
        public static bool IsAjaxRequest(this HttpRequest request)
        {
            if (request == null)
                throw new ArgumentNullException("request");

            if (request.Headers != null)
                return request.Headers["X-Requested-With"] == "XMLHttpRequest";
            return false;
        }
    }
}