namespace Webu.Core.Authorize
{
    public enum PermissionAction
    {
        Read,
        Create,
        LoginRequired
    }
}