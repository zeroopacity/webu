using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Webu.Core;

namespace Webu.Core.Authorize
{
    public class AuthorizeActionFilter : IAsyncActionFilter
    {
        private readonly PermissionItem _item;
        private readonly PermissionAction _action;
        public AuthorizeActionFilter(PermissionItem item, PermissionAction action)
        {
            _item = item;
            _action = action;
        }
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            bool isAuthorized = context.HttpContext.Session.Get<bool>("IsAuthorize");

            if (!isAuthorized)
            {
                if (context.HttpContext.Request.IsAjaxRequest())
                {
                    context.Result = new UnauthorizedResult();
                }
                else
                {
                    context.Result = new RedirectToActionResult("Login", "Account", new { });
                }
            }
            else
            {
                await next();
            }
        }
    }
}