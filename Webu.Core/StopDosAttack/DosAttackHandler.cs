using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System.Threading;
using System.Threading.Tasks;
using System;

namespace Webu.Core.StopDosAttack
{
    #region Using



    #endregion

    /// <summary>
    /// Block the response to attacking IP addresses.
    /// </summary>
    public class DosAttackMiddleware
    {
        private static Dictionary<string, short> _IpAddresses = new Dictionary<string, short>();
        private static Stack<string> _Banned = new Stack<string>();
        private static Timer _Timer;// = CreateTimer();
        private static Timer _BannedTimer;// = CreateBanningTimer();

        private readonly RequestDelegate _next;
        public DosAttackMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        private const int BANNED_REQUESTS = 10;
        private const int REDUCTION_INTERVAL = 1000; // 1 second
        private const int RELEASE_INTERVAL = 5 * 60 * 1000; // 5 minutes

        public async Task Invoke(HttpContext context)
        {
            string ip = context.Request.Host.Host;
            if (_Banned.Contains(ip))
            {
                context.Response.StatusCode = 403;
                return;
            }
            else
            {
                CheckIpAddress(ip);
                await _next.Invoke(context);
            }
        }

        /// <summary>
        /// Checks the requesting IP address in the collection
        /// and bannes the IP if required.
        /// </summary>
        private static void CheckIpAddress(string ip)
        {
            if (!_IpAddresses.ContainsKey(ip))
            {
                _IpAddresses[ip] = 1;
            }
            else if (_IpAddresses[ip] == BANNED_REQUESTS)
            {
                _Banned.Push(ip);
                _IpAddresses.Remove(ip);
            }
            else
            {
                _IpAddresses[ip]++;
            }
        }

        #region Timers

        /// <summary>
        /// Creates the timer that substract a request
        /// from the _IpAddress dictionary.
        /// </summary>
        private static Timer CreateTimer()
        {
            Timer timer = GetTimer(REDUCTION_INTERVAL);
            return timer;
        }

        /// <summary>
        /// Creates the timer that removes 1 banned IP address
        /// everytime the timer is elapsed.
        /// </summary>
        /// <returns></returns>
        private static Timer CreateBanningTimer()
        {
            Timer timer = GetTimer(RELEASE_INTERVAL);
            return timer;
        }

        /// <summary>
        /// Creates a simple timer instance and starts it.
        /// </summary>
        /// <param name="interval">The interval in milliseconds.</param>
        private static Timer GetTimer(int interval)
        {
            var _autoEvent = new AutoResetEvent(true);
            Timer timer = new Timer(Execute, _autoEvent, 0, interval);
            return timer;
        }

        /// <summary>
        /// Substracts a request from each IP address in the collection.
        /// </summary>
        private static void Execute(Object stateInfo)
        {
            _Banned.Pop();
            foreach (string key in _IpAddresses.Keys)
            {
                _IpAddresses[key]--;
                if (_IpAddresses[key] == 0)
                    _IpAddresses.Remove(key);
            }
        }

        #endregion

    }
    public static class DosAttackMiddlewareExtension
    {
        public static void MapDosAttackHandler(this IApplicationBuilder app)
        {
           app.UseMiddleware<DosAttackMiddleware>();
        }
    }
}