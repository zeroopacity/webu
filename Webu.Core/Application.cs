/**
    App holds the application wide available objects
 */
using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

namespace Webu.Core
{
    public class Application
    {
        private static Application _instance;

        private Application(IConfiguration _config, IHostingEnvironment _hostingEnv)
        {
            Config = _config;
            hostingEnv = _hostingEnv;
        }
        public static IConfiguration Config { get; private set; }
        public static IHostingEnvironment hostingEnv;

        public static void CreateInstance(IConfiguration _config, IHostingEnvironment _hostingEnv)
        {
            if (Application._instance == null)
            {
                Application._instance = new Application(_config, _hostingEnv);
            }
            else
            {
                throw new System.Exception("Application instance is already created");
            }
        }
    }
}