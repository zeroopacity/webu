/**
 * Scripts/app.js file
 */
var app = (function () {
    var self = {
        components: {},
        utils: {
            async: function (func) {
                setTimeout(func, 0);
            },
            logger: function (str) {
                console.log(str);
            }
        }
    };
    return self;
}());

$(document).ready(function () {
    app.$if.init();
});
/**
 * Instance Factory
 * Helps in creating component instances
 * Run Garbage collector periodically to check if instance can be garbage collected to free memory
 * and turn off any event subscriptions
 */
app.$if = (function () {
    var _componentRepository = {},
        _lastKey = 0,
        _elementsToUnbind = [],
        _holdGarbageCollection = false;
    _elementsToUnbind.lock = true;

    var baseComponent = (function () {
        var baseClass = function () { };
        baseClass.prototype.init = function () { };
        baseClass.prototype.destroy = function () { };
        baseClass.prototype.on = function (event, callback) {
            app.$ps.on(event, callback, this);
            this.subscriptions = (this.subscriptions || []);
            this.subscriptions.push(event);
        };
        baseClass.prototype.off = function (event) {
            app.$ps.off(event, this);
        };
        baseClass.prototype.publish = function (event, params) {
            app.$ps.publish(event, params);
        };
        baseClass.prototype.completeOff = function () {
            if (this.subscriptions) {
                while (this.subscriptions.length) {
                    this.off(this.subscriptions.pop(), this);
                }
            }
        };
        return baseClass;
    }());

    var $gc = function () {
        var interval = setInterval(function () {
            if (!_holdGarbageCollection && _elementsToUnbind.length && !_elementsToUnbind.lock) {
                removeObjects();
            }
        }, 2000);
        function removeObjects() {
            while (_elementsToUnbind.length) {
                var key = _elementsToUnbind.pop();
                var obj = _componentRepository[key];
                if (obj) {
                    obj.completeOff();
                    obj.destroy();
                    if (obj.events) {
                        for (var i = 0; i < obj.events.length; i++) {
                            var selectedElements = element.querySelectorAll(obj.events[i].selector),
                                selectedElementsEvents = obj.events[i].events.split(',');

                            selectedElements.forEach(function (selectedElement) {
                                for (var j = 0; j < selectedElementsEvents.length; j++) {
                                    selectedElement.removeEventListener(selectedElementsEvents[j], obj.events[i].handler);
                                }
                            });
                        }
                    }
                    delete _componentRepository[key];
                }
            }
            _elementsToUnbind.lock = true;
        }
    };

    return {
        init: function () {
            var self = this;
            document.querySelectorAll("[data-component]").forEach(function (element) {
                self.createCInstance(element);
            });

            $(document).bind("DOMNodeRemoved", function (e) {
                var instanceKey = e.target.dataset.instance;

                //pause gc if list in being populated
                if (_elementsToUnbind.lock) {
                    setTimeout(function () {
                        _elementsToUnbind.lock = false;
                    }, 200);
                }
                if (instanceKey) {
                    _elementsToUnbind.lock = true;
                    _elementsToUnbind.push(instanceKey);
                    var childComponents = $(e.target).find("[data-component]");
                    if (childComponents.length) {
                        $.each(childComponents, function (i, childComponent) {
                            var childInstanceKey = childComponent.dataset.instance;
                            _elementsToUnbind.push(childInstanceKey);
                        });
                    }
                }
            });
            $gc();
        },
        getInstance: function (key) {
            return _componentRepository[key];
        },
        createCInstance: function (element) {
            var componentClass = app.components[element.getAttribute("data-component")];
            if (componentClass) {
                componentClass.prototype = Object.create(baseComponent.prototype);
                componentClass.prototype.constructor = componentClass;
                var obj = new componentClass();

                var instanceKey = "key_" + (_lastKey++);
                obj.key = instanceKey;
                _componentRepository[instanceKey] = obj;
                element.setAttribute("data-instance", instanceKey);
                obj.init(element);

                if (obj.events) {
                    for (var i = 0; i < obj.events.length; i++) {
                        var selectedElements = element.querySelectorAll(obj.events[i].selector),
                            selectedElementsEvents = obj.events[i].events.split(',');
                        selectedElements.forEach(function (selectedElement) {
                            for (var j = 0; j < selectedElementsEvents.length; j++) {
                                selectedElement.addEventListener(selectedElementsEvents[j], obj.events[i].handler);
                            }
                        });
                    }
                }
            }
            else {
                app.utils.logger.logError(componentClass + "  can not be instantiated");
            }
        },
        createCInstance2: function ($html) {
            var self = this;
            var $element = $html.first();
            var parentComponent = $element.data("component");
            if (parentComponent) {
                self.createCInstance($element.get(0));
            }
            $element.find("[data-component]").each(function (i, element) {
                self.createCInstance(element);
            });
        },
        muteGC: function (mute) {
            _holdGarbageCollection = mute;
        }
    };
}());
/**
 * $ps is messaging module between different components
 */
app.$ps = (function () {
    var mediator = {};
    var system = {
        on: function (event, callback, obj) {
            if (!mediator[event]) {
                mediator[event] = {
                    subscribers: []
                };
            }
            mediator[event].subscribers.push({
                callback: callback,
                obj: obj
            });
        },
        off: function (event, obj) {
            var subscribers = mediator[event].subscribers;
            if (subscribers) {
                var index;
                for (var i = 0; i < subscribers.length; i++) {
                    if (subscribers[i].obj.key === obj.key) {
                        index = i;
                        break;
                    }
                }
                //remove listener from subscribers list
                if (index >= 0) {
                    subscribers.splice(index, 1);
                }

                //delete group if no subscriber is present
                if (!subscribers.length) {
                    delete mediator[event];
                }
            }
        },
        publish: function (event, params) {
            var event = mediator[event];
            if (event) {
                var listeners = event.subscribers;
                for (var i = 0; i < listeners.length; i++) {
                    listeners[i].callback.call(listeners[i].obj, params);
                }
            }
        },
    };
    return system;
}());
this["webu"] = this["webu"] || {};

this["webu"]["CreatePostTemplate"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<form method=\"post\" enctype=\"multipart/form-data\" action=\"/Home/CreatePost\">\r\n    <label>Upload Img</label>\r\n    <input name=\"PostImage\" type=\"file\" />\r\n    <button type=\"submit\">Save</button>\r\n</form>\r\n\r\n<div class=\"dropdown\">\r\n    <button class=\"dropdown-btn\">Dropdown</button>\r\n    <div class=\"dropdown-content\">\r\n        <a href=\"#\">Link 1</a>\r\n        <a href=\"#\">Link 2</a>\r\n        <a href=\"#\">Link 3</a>\r\n    </div>\r\n</div>";
},"useData":true});

this["webu"]["LoginTemplate"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<form action=\"/account/login\" method=\"post\">\r\n    UserName<br>\r\n    <input name=\"UserName\" type=\"text\" />\r\n    <button type=\"submit\">Login</button>\r\n</form>";
},"useData":true});

this["webu"]["post-template-1"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=container.escapeExpression;

  return "<div class=\"post\" data-component=\"post\">\r\n    <div class=\"post_header\">\r\n        "
    + alias1(((helper = (helper = helpers.From || (depth0 != null ? depth0.From : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"From","hash":{},"data":data}) : helper)))
    + "\r\n    </div>\r\n    <div class=\"post_content\">\r\n        "
    + alias1(container.lambda(((stack1 = (depth0 != null ? depth0.Content : depth0)) != null ? stack1.Data : stack1), depth0))
    + "\r\n    </div>\r\n    <div class=\"post_footer\">\r\n    </div>\r\n</div>";
},"useData":true});

this["webu"]["WebSocketComponent"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<h1>WebSocket Sample Application</h1>\r\n<p id=\"stateLabel\">Ready to connect...</p>\r\n<div>\r\n    <label for=\"connectionUrl\">WebSocket Server URL:</label>\r\n    <input id=\"connectionUrl\" />\r\n    <button id=\"connectButton\" type=\"submit\">Connect</button>\r\n</div>\r\n<p></p>\r\n<div>\r\n    <label for=\"sendMessage\">Message to send:</label>\r\n    <input id=\"sendMessage\" disabled />\r\n    <button id=\"sendButton\" type=\"submit\" disabled>Send</button>\r\n    <button id=\"closeButton\" disabled>Close Socket</button>\r\n</div>\r\n\r\n<h2>Communication Log</h2>\r\n<table style=\"width: 800px\">\r\n    <thead>\r\n        <tr>\r\n            <td style=\"width: 100px\">From</td>\r\n            <td style=\"width: 100px\">To</td>\r\n            <td>Data</td>\r\n        </tr>\r\n    </thead>\r\n    <tbody id=\"commsLog\">\r\n    </tbody>\r\n</table>";
},"useData":true});

this["webu"]["SearchTemplate"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<form action=\"/search\" method=\"POST\">\r\n    <label>Search Keywords</label>\r\n    <input type=\"text\" class=\"search-input\" />\r\n    <input type=\"button\" class=\"search-submit\" value=\"search\"/>\r\n</form>";
},"useData":true});

this["webu"]["userWallPost"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "";
},"useData":true});
app.components.CreatePostComponent = function () {
    var template;
    this.init = function (element) {
        template = webu.CreatePostTemplate();
        element.innerHTML = template;
    };
};
/**
 * Header component
 */
app.components.header = function () {
    var self = this;
    function showSearchWindow() {
        self.publish("show_search_window");
    }
    this.events = [
        { events: 'click', selector: ".search-link", handler: showSearchWindow }
    ];
    this.init = function (element) {
    };
    this.destroy = function () {
        alert("I am about to destroy....");
    };
};
app.components.LoginComponent = function(){
    this.init = function(element){
        element.innerHTML = webu.LoginTemplate();
    };
};
app.components.post = function () {
    var element;
    //region private
    function changeColor(e) {
        e.currentTarget.style.backgroundColor = "grey";
    }
    //end

    //region public
    this.init = function (node) {
        element = node;
        element.addEventListener("click", changeColor);
    };
    this.destroy = function () {
        console.log("I am being removed");
        element.removeEventListener("click", changeColor);
    };
    //end
};
/**
 * postComponent
 */
app.components.postComponent = function () {
    this.init = function (element) {
        var $element = $(element);
        app.utils.async(function () {
            for (var i = 0; i < 1; i++) {
                var template1 = webu["post-template-1"]({
                    From: "Vijay" + i,
                    Content: {
                        Type: "Text",
                        Data: "Test Post" + i
                    }
                });

                var post  = $(template1);
                $element.append(post);
                app.$if.createCInstance2(post);
            }
        });
    };
};
app.components.WebSocketComponent = function () {
    this.init = function (element) {
        element.innerHTML = webu.WebSocketComponent();

        var connectionForm = document.getElementById("connectionForm");
        var connectionUrl = document.getElementById("connectionUrl");
        var connectButton = document.getElementById("connectButton");
        var stateLabel = document.getElementById("stateLabel");
        var sendMessage = document.getElementById("sendMessage");
        var sendButton = document.getElementById("sendButton");
        var sendForm = document.getElementById("sendForm");
        var commsLog = document.getElementById("commsLog");
        var socket;
        var scheme = document.location.protocol == "https:" ? "wss" : "ws";
        var port = document.location.port ? (":" + document.location.port) : "";
        connectionUrl.value = scheme + "://" + document.location.hostname + port + "/ws";
        function updateState() {
            function disable() {
                sendMessage.disabled = true;
                sendButton.disabled = true;
                closeButton.disabled = true;
            }
            function enable() {
                sendMessage.disabled = false;
                sendButton.disabled = false;
                closeButton.disabled = false;
            }
            connectionUrl.disabled = true;
            connectButton.disabled = true;
            if (!socket) {
                disable();
            } else {
                switch (socket.readyState) {
                    case WebSocket.CLOSED:
                        stateLabel.innerHTML = "Closed";
                        disable();
                        connectionUrl.disabled = false;
                        connectButton.disabled = false;
                        break;
                    case WebSocket.CLOSING:
                        stateLabel.innerHTML = "Closing...";
                        disable();
                        break;
                    case WebSocket.CONNECTING:
                        stateLabel.innerHTML = "Connecting...";
                        disable();
                        break;
                    case WebSocket.OPEN:
                        stateLabel.innerHTML = "Open";
                        enable();
                        break;
                    default:
                        stateLabel.innerHTML = "Unknown WebSocket State: " + socket.readyState;
                        disable();
                        break;
                }
            }
        }
        closeButton.onclick = function () {
            if (!socket || socket.readyState != WebSocket.OPEN) {
                alert("socket not connected");
            }
            socket.close(1000, "Closing from client");
        };
        sendButton.onclick = function () {
            if (!socket || socket.readyState != WebSocket.OPEN) {
                alert("socket not connected");
            }
            var data = sendMessage.value;
            socket.send(data);
            commsLog.innerHTML += '<tr>' +
                '<td class="commslog-client">Client</td>' +
                '<td class="commslog-server">Server</td>' +
                '<td class="commslog-data">' + data + '</td>' +
                '</tr>';
        };
        connectButton.onclick = function () {
            stateLabel.innerHTML = "Connecting";
            socket = new WebSocket(connectionUrl.value);
            socket.onopen = function (event) {
                updateState();
                commsLog.innerHTML += '<tr>' +
                    '<td colspan="3" class="commslog-data">Connection opened</td>' +
                    '</tr>';
            };
            socket.onclose = function (event) {
                updateState();
                commsLog.innerHTML += '<tr>' +
                    '<td colspan="3" class="commslog-data">Connection closed. Code: ' + event.code + '. Reason: ' + event.reason + '</td>' +
                    '</tr>';
            };
            socket.onerror = updateState;
            socket.onmessage = function (event) {
                commsLog.innerHTML += '<tr>' +
                    '<td class="commslog-server">Server</td>' +
                    '<td class="commslog-client">Client</td>' +
                    '<td class="commslog-data">' + event.data + '</td>' +
                '</tr>';
            };
        };
    };
};
app.components.SearchComponent = function () {
    var template, element;
    this.init = function (ele) {
        element = ele;
        this.on("show_search_window", showSearchWindow);
        element.addEventListener("click", function(){
            element.className = "search";
        });
    };


    function showSearchWindow() {
        if (!template) {
            template = webu.SearchTemplate();
            element.innerHTML += template;
        }
        element.className = element.className + " " + "active";
    }
};
app.components.sideNav = function () {
    var data;
    this.init = function (element) {
    };
};