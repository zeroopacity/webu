﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using Webu.Core.WebSockets;
using Webu.Core;

namespace Webu
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();

            //Initialize application wide instance
            Application.CreateInstance(Configuration, env);
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            #region Session
            //services.AddDistributedMemoryCache();
            // Add framework services.
            services.AddDistributedRedisCache(option =>
            {
                option.InstanceName = "Sample";
                option.Configuration = Application.Config["RedisConnection"];
            });
            services.AddSession(options =>
            {
                // Set a short timeout for easy testing.
                options.IdleTimeout = TimeSpan.FromSeconds(Int32.Parse(Application.Config["SessionTimeout"]));
                options.CookieName = ".Webu.Session";
                options.CookieHttpOnly = true;
                //options.CookieSecure = Microsoft.AspNetCore.Http.CookieSecurePolicy.Always;
            });
            #endregion
            services.AddMvc();
            services.AddWebSocketManager();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IServiceProvider serviceProvider)
        {
            app.UseSession();
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();

                //for testing
                //app.MapDosAttackHandler();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                //app.MapDosAttackHandler();
            }

            app.UseStaticFiles();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            #region webSockets
            var webSocketOptions = new WebSocketOptions()
            {
                KeepAliveInterval = TimeSpan.FromSeconds(120),
                ReceiveBufferSize = 4 * 1024
            };
            app.UseWebSockets(webSocketOptions);

            app.MapWebSocketManager("/ws", serviceProvider.GetService<ChatMessageHandler>());
            #endregion
        }
    }
}
