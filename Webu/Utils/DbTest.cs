using ServiceStack.Redis;
using Webu.Core;
using System;

namespace Webu.Utils
{
    public class DbTest
    {
        void TestRedis()
        {
            var manager = new RedisManagerPool(
                Application.Config["RedisConnection:Domain"] +
                ":" + Application.Config["RedisConnection:Port"]
                );
            using (var client = manager.GetClient())
            {
                client.Set("foo", "bar");
                Console.WriteLine("foo={0}", client.Get<string>("foo"));
            }
        }
    }
}