using System;
using System.IO;
using SkiaSharp;
using Webu.Core;

namespace Webu.Utils
{
    public class ThumbnailGenerator
    {
        const int size = 150;
        const int quality = 75;
        public string GenerateThumbnail(string inputPath, string fileName)
        {
            using (var input = File.OpenRead(inputPath))
            {
                using (var inputStream = new SKManagedStream(input))
                {
                    using (var original = SKBitmap.Decode(inputStream))
                    {
                        int width, height;
                        if (original.Width > original.Height)
                        {
                            width = size;
                            height = original.Height * size / original.Width;
                        }
                        else
                        {
                            width = original.Width * size / original.Height;
                            height = size;
                        }

                        using (var resized = original.Resize(new SKImageInfo(width, height), SKBitmapResizeMethod.Lanczos3))
                        {
                            if (resized == null) return null;
                            using (var image = SKImage.FromBitmap(resized))
                            {
                                var tFileFullName = Application.hostingEnv.WebRootPath + $@"\uploads\thumb-{fileName}";
                                using (var output = File.OpenWrite(tFileFullName))
                                {
                                    image.Encode(SKEncodedImageFormat.Jpeg, quality).SaveTo(output);
                                }
                                return tFileFullName;
                            }
                        }
                    }
                }
            }
        }
    }
}