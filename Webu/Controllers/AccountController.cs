using Microsoft.AspNetCore.Mvc;
using Webu.Core;
using Webu.Entity;

namespace Webu.Controllers
{
    public class AccountController : BaseController
    {
        [HttpGet]
        public IActionResult Login()
        {
            if (HttpContext.Session.Get<bool>("IsAuthorize"))
            {
                return Redirect("/home");
            }
            return View();
        }
        [HttpPost]
        public IActionResult Login(User user)
        {
            HttpContext.Session.Set<bool>("IsAuthorize", true);
            return Redirect("/home");
        }
    }
}