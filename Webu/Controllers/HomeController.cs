﻿using Microsoft.AspNetCore.Mvc;
using Webu.Core;
using System.IO;
using Webu.Utils;
using Webu.Database.Mongo;
using Webu.Core.Authorize;

namespace Webu.Controllers
{
    public class HomeController : BaseController
    {
        [Authorize(PermissionItem.User, PermissionAction.LoginRequired)]
        public IActionResult Index()
        {
            var userRepository = new UserRepository();
            // if(HttpContext.Session.Get<string>("Name") == null){
            //     HttpContext.Session.Set<string>("Name", Request.QueryString("name"));
            // }
            return View();
        }

        public IActionResult CreatePost()
        {
            var file = Request.Form.Files.GetFile("PostImage");
            var fullFilename = Application.hostingEnv.WebRootPath + $@"\uploads\{file.FileName}";
            using (FileStream fs = System.IO.File.Create(fullFilename))
            {
                file.CopyTo(fs);
                fs.Flush();
            }
            ThumbnailGenerator tg = new ThumbnailGenerator();
            tg.GenerateThumbnail(fullFilename, file.FileName);
            return new RedirectToActionResult("Index", "Home", new { });
        }
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        [Authorize(PermissionItem.User, PermissionAction.LoginRequired)]
        public IActionResult Contact()
        {
            return new JsonResult(new { Name = "Vijay", Email = "vsingh123@sapient.com" });
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
