/**
 * Grunt config file
 */
module.exports = function(grunt) {
    grunt.initConfig({
        jshint: {
            files: ['Web.UI/scripts/**/*.js', "!Web.UI/scripts/core/**/*.js", '!Web.UI/scripts/templates.js',"!Web.UI/scripts/build/*.js"],
            options: {
                globals: {
                    jQuery: true,
                    window: true,
                    asi : false
                }
            }
        },
        sass: {                              // Task
            dist: {                            // Target
                options: {                       // Target options
                    style: 'expanded'
                },
                files: {                         // Dictionary of files
                    'Webu/wwwroot/css/site.css': 'Web.UI/css/site.scss'       // 'destination': 'source'
                }
            }
        },
        concat: {
            options: {
                separator: '\n',
            },
            devBuild_libs: {
                src: [
                    'Web.UI/scripts/core/libs/jQuery-1.10.js',
                    'Web.UI/scripts/core/libs/handlebars.js'],
                    //'Web.UI/scripts/core/libs/router.js'],
                dest: 'Webu/wwwroot/scripts/libs.js',
            },
            devBuild_apps: {
                src: [
                    'Web.UI/scripts/app.js',
                    'Web.UI/scripts/core/utils/*.js',
                    'Web.UI/scripts/templates.js',
                    'Web.UI/scripts/components/**/*.js'],
                    //'Web.UI/scripts/registerRoutes.js'
                dest: 'Webu/wwwroot/scripts/app.js',
            }
        },
        uglify: {
            options: {
                preserveComments:'all',
                mangle:false,
                compress:false,
                sourceMap: false,
                compress: {
                    drop_console: true
                },
                beautify: {
                    beautify: true
                }
            },
            js: {
                files: {
                    'Webu/wwwroot/scripts/libs.js': ['Web.UI/scripts/build/libs.js'],
                    'Webu/wwwroot//scripts/app.js': ['Web.UI/scripts/build/app.js'],
                }
            }
        },
        clean: {
            build: [
                    'Webu/wwwroot/css/*.css',
                    'Webu/wwwroot/scripts/*.js',
                    "Web.UI/scripts/build"],
        },
        cacheBust: {
            script: {
                options: {
                    assets: ['app.js', 'libs.js'],
                    queryString: true,
                    baseDir: './Webu/wwwroot/scripts/',
                    deleteOriginals: false
                },
                src: ['Webu/Views/Shared/_Layout.cshtml'],
                queryString: true
            },
            css: {
                options: {
                    assets: ['site.css'],
                    queryString: true,
                    baseDir: './Webu/wwwroot/scripts/',
                    deleteOriginals: false
                },
                src: ['Webu/Views/Shared/_Layout.cshtml']
            },
        },
        watch: {
            scripts: {
                files: ['Web.UI/scripts/**/*.js', '!Web.UI/scripts/core/libs/*.js'],
                tasks: ['jshint','concat:devBuild_libs', 'concat:devBuild_apps']
            },
            templates: {
                files: ['Web.UI/scripts/**/*.html'],
                tasks: ['handlebars','concat:devBuild']
            },
            css: {
                files: ['Web.UI/css/**/*.scss'],
                tasks: ['sass']
            },
        },
        handlebars: {
            compile: {
                options: {
                    namespace: 'webu',
                    processName: function(filePath) {
                        var pieces = filePath.split('/');
                        return pieces[pieces.length - 1].split(".")[0];
                    }
                },
                files: {
                    'Web.UI/scripts/templates.js': ['Web.UI/scripts/**/*.html']
                }
            }
        }
    });

grunt.loadNpmTasks('grunt-contrib-jshint');
grunt.loadNpmTasks('grunt-contrib-watch');
grunt.loadNpmTasks('grunt-contrib-handlebars');
grunt.loadNpmTasks('grunt-contrib-sass');
grunt.loadNpmTasks('grunt-cache-bust');
grunt.loadNpmTasks('grunt-contrib-uglify');
grunt.loadNpmTasks('grunt-contrib-clean');
grunt.loadNpmTasks('grunt-contrib-concat');

grunt.registerTask('build-dev', ['clean','jshint', 'sass', 'handlebars', 'concat:devBuild_libs', 'concat:devBuild_apps', 'watch']);
grunt.registerTask('build-prod', ['clean', 'jshint', 'sass', 'handlebars', 'concat', 'uglify', 'cacheBust']);
};