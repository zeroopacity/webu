﻿/**
 * Header component
 */
app.components.header = function () {
    var self = this;
    function showSearchWindow() {
        self.publish("show_search_window");
    }
    this.events = [
        { events: 'click', selector: ".search-link", handler: showSearchWindow }
    ];
    this.init = function (element) {
    };
    this.destroy = function () {
        alert("I am about to destroy....");
    };
};