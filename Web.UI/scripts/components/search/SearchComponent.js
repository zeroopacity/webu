app.components.SearchComponent = function () {
    var template, element;
    this.init = function (ele) {
        element = ele;
        this.on("show_search_window", showSearchWindow);
        element.addEventListener("click", function(){
            element.className = "search";
        });
    };


    function showSearchWindow() {
        if (!template) {
            template = webu.SearchTemplate();
            element.innerHTML += template;
        }
        element.className = element.className + " " + "active";
    }
};