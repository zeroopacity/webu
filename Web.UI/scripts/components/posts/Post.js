app.components.post = function () {
    var element;
    //region private
    function changeColor(e) {
        e.currentTarget.style.backgroundColor = "grey";
    }
    //end

    //region public
    this.init = function (node) {
        element = node;
        element.addEventListener("click", changeColor);
    };
    this.destroy = function () {
        console.log("I am being removed");
        element.removeEventListener("click", changeColor);
    };
    //end
};