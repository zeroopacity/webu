/**
 * postComponent
 */
app.components.postComponent = function () {
    this.init = function (element) {
        var $element = $(element);
        app.utils.async(function () {
            for (var i = 0; i < 1; i++) {
                var template1 = webu["post-template-1"]({
                    From: "Vijay" + i,
                    Content: {
                        Type: "Text",
                        Data: "Test Post" + i
                    }
                });

                var post  = $(template1);
                $element.append(post);
                app.$if.createCInstance2(post);
            }
        });
    };
};