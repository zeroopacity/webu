'use strict';

var Router = (function() {
    var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function(obj) {
        return typeof obj;
    } : function(obj) {
        return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj;
    };

    function _toConsumableArray(arr) {
        if (Array.isArray(arr)) {
            for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) {
                arr2[i] = arr[i];
            }
            return arr2;
        } else {
            return Array.from(arr);
        }
    }

    function isPushStateAvailable() {
        return !!(typeof window !== 'undefined' && window.history && window.history.pushState);
    }

    function Router(r, useHash, element, hash) {
        this.root = null;
        this._routes = [];
        this._useHash = useHash;
        this._hash = typeof hash === 'undefined' ? '#' : hash;
        this._paused = false;
        this._destroyed = false;
        this._lastRouteResolved = null;
        this._notFoundHandler = null;
        this._defaultHandler = null;
        this._usePushState = !useHash && isPushStateAvailable();
        this._onLocationChange = this._onLocationChange.bind(this);

        if (r) {
            this.root = useHash ? r.replace(/\/$/, '/' + this._hash) : r.replace(/\/$/, '');
        } else if (useHash) {
            this.root = this._cLoc().split(this._hash)[0].replace(/\/$/, '/' + this._hash);
        }

        this._listen();
        this.updatePageLinks();
    }

    function clean(s) {
        if (s instanceof RegExp) return s;
        return s.replace(/\/+$/, '').replace(/^\/+/, '/');
    }

    function regExpResultToParams(match, names) {
        if (names.length === 0) return null;
        if (!match) return null;
        return match.slice(1, match.length).reduce(function(params, value, index) {
            if (params === null) params = {};
            params[names[index]] = value;
            return params;
        }, null);
    }

    function replaceDynamicURLParts(route) {
        var paramNames = [],
            regexp;

        if (route instanceof RegExp) {
            regexp = route;
        } else {
            regexp = new RegExp(clean(route).replace(Router.PARAMETER_REGEXP, function(full, dots, name) {
                paramNames.push(name);
                return Router.REPLACE_VARIABLE_REGEXP;
            }).replace(Router.WILDCARD_REGEXP, Router.REPLACE_WILDCARD) + Router.FOLLOWED_BY_SLASH_REGEXP, Router.MATCH_REGEXP_FLAGS);
        }
        return {
            regexp: regexp,
            paramNames: paramNames
        };
    }

    function getUrlDepth(url) {
        return url.replace(/\/$/, '').split('/').length;
    }

    function compareUrlDepth(urlA, urlB) {
        return getUrlDepth(urlB) - getUrlDepth(urlA);
    }

    function findMatchedRoutes(url) {
        var routes = arguments.length <= 1 || arguments[1] === undefined ? [] : arguments[1];

        return routes.map(function(route) {
            var _replaceDynamicURLPar = replaceDynamicURLParts(route.route);

            var regexp = _replaceDynamicURLPar.regexp;
            var paramNames = _replaceDynamicURLPar.paramNames;

            var match = url.match(regexp);
            var params = regExpResultToParams(match, paramNames);

            return match ? {
                match: match,
                route: route,
                params: params
            } : false;
        }).filter(function(m) {
            return m;
        });
    }

    function match(url, routes) {
        return findMatchedRoutes(url, routes)[0] || false;
    }

    function root(url, routes) {
        var matched = findMatchedRoutes(url, routes.filter(function(route) {
            var u = clean(route.route);

            return u !== '' && u !== '*';
        }));
        var fallbackURL = clean(url);

        if (matched.length > 0) {
            return matched.map(function(m) {
                return clean(url.substr(0, m.match.index));
            }).reduce(function(root, current) {
                return current.length < root.length ? current : root;
            }, fallbackURL);
        }
        return fallbackURL;
    }

    function isHashChangeAPIAvailable() {
        return !!(typeof window !== 'undefined' && 'onhashchange' in window);
    }

    function extractGETParameters(url) {
        return url.split(/\?(.*)?$/).slice(1).join('');
    }

    function getOnlyURL(url, useHash, hash) {
        var onlyURL = url.split(/\?(.*)?$/)[0];

        if (typeof hash === 'undefined') {
            // To preserve BC
            hash = '#';
        }

        if (isPushStateAvailable() && !useHash) {
            onlyURL = onlyURL.split(hash)[0];
        }

        return onlyURL;
    }

    function manageHooks(handler, route, params) {
        if (route && route.hooks && _typeof(route.hooks) === 'object') {
            if (route.hooks.after) {
                handler();
                route.hooks.after && route.hooks.after(params);
            }
            return;
        }
        handler();
    };

    Router.prototype = {
        helpers: {
            match: match,
            root: root,
            clean: clean
        },
        navigate: function navigate(path, absolute) {
            var to,
                self = this,
                path = path || '';

            if (this._usePushState) {
                to = (!absolute ? this._getRoot() + '/' : '') + path.replace(/^\/+/, '/');
                to = to.replace(/([^:])(\/{2,})/g, '$1/');
                if (this.globalHooks.before) {
                    var params = match(path, this._routes)
                    this.globalHooks.before(params, function(navigate) {
                        if (navigate) {
                            history[self._paused ? 'replaceState' : 'pushState']({}, '', to);
                            self.resolve();
                        }
                    });
                } else {
                    history[self._paused ? 'replaceState' : 'pushState']({}, '', to);
                    self.resolve();
                }
            } else if (typeof window !== 'undefined') {
                if (this.globalHooks.before) {
                    var params = match(path, this._routes)
                    this.globalHooks.before(params, function(navigate) {
                        if (navigate) {
                            path = path.replace(new RegExp('^' + self._hash), '');
                            window.location.href = window.location.href.replace(new RegExp('(#|' + self._hash + ')(.*)$'), '') + self._hash + path;
                        }
                    });
                } else {
                    path = path.replace(new RegExp('^' + self._hash), '');
                    window.location.href = window.location.href.replace(new RegExp('(#|' + self._hash + ')(.*)$'), '') + self._hash + path;
                }
            }
            return this;
        },
        on: function on() {
            var _this = this;

            for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
                args[_key] = arguments[_key];
            }

            if (typeof args[0] === 'function') {
                this._defaultHandler = {
                    handler: args[0],
                    hooks: args[1]
                };
            } else if (args.length >= 2) {
                if (args[0] === '/') {
                    var func = args[1];

                    if (_typeof(args[1]) === 'object') {
                        func = args[1].uses;
                    }

                    this._defaultHandler = {
                        handler: func,
                        hooks: args[2]
                    };
                } else {
                    this._add(args[0], args[1], args[2], args[3]);
                }
            } else if (_typeof(args[0]) === 'object') {
                var orderedRoutes = Object.keys(args[0]).sort(compareUrlDepth);

                orderedRoutes.forEach(function(route) {
                    _this.on(route, args[0][route].controller, args[0][route].config, _this.globalHooks);
                });
            }
            return this;
        },
        off: function off(handler) {
            if (this._defaultHandler !== null && handler === this._defaultHandler.handler) {
                this._defaultHandler = null;
            } else if (this._notFoundHandler !== null && handler === this._notFoundHandler.handler) {
                this._notFoundHandler = null;
            }
            this._routes = this._routes.reduce(function(result, r) {
                if (r.handler !== handler) result.push(r);
                return result;
            }, []);
            return this;
        },
        notFound: function notFound(handler, hooks) {
            this._notFoundHandler = {
                handler: handler,
                hooks: hooks
            };
            return this;
        },
        resolve: function resolve(current) {
            var _this2 = this;

            var handler, m;
            var url = (current || this._cLoc()).replace(this._getRoot(), '');

            if (this._useHash) {
                url = url.replace(new RegExp('^\/' + this._hash), '/');
            }

            var GETParameters = extractGETParameters(current || this._cLoc());
            var onlyURL = getOnlyURL(url, this._useHash, this._hash);

            if (this._paused || this._lastRouteResolved && onlyURL === this._lastRouteResolved.url && GETParameters === this._lastRouteResolved.query) {
                return false;
            }

            m = match(onlyURL, this._routes);

            if (m) {
                this._lastRouteResolved = {
                    url: onlyURL,
                    query: GETParameters
                };
                handler = m.route.handler;
                manageHooks(function() {
                    m.route.route instanceof RegExp ? handler.apply(undefined, _toConsumableArray(m.match.slice(1, m.match.length))) : handler(m.params, GETParameters);
                }, m.route, m.params);
                return m;
            } else if (this._defaultHandler && (onlyURL === '' || onlyURL === '/' || onlyURL === this._hash)) {
                manageHooks(function() {
                    _this2._lastRouteResolved = {
                        url: onlyURL,
                        query: GETParameters
                    };
                    _this2._defaultHandler.handler(GETParameters);
                }, this._defaultHandler);
                return true;
            } else if (this._notFoundHandler) {
                manageHooks(function() {
                    _this2._lastRouteResolved = {
                        url: onlyURL,
                        query: GETParameters
                    };
                    _this2._notFoundHandler.handler(GETParameters);
                }, this._notFoundHandler);
            }
            return false;
        },
        destroy: function destroy() {
            this._routes = [];
            this._destroyed = true;
            clearTimeout(this._listeningInterval);
            if (typeof window !== 'undefined') {
                window.removeEventListener('popstate', this._onLocationChange);
                window.removeEventListener('hashchange', this._onLocationChange);
            }
        },
        updatePageLinks: function updatePageLinks() {
            var self = this;

            if (typeof document === 'undefined') return;

            this._findLinks().forEach(function(link) {
                if (!link.hasListenerAttached) {
                    link.addEventListener('click', function(e) {
                        var location = link.getAttribute('href');

                        if (!self._destroyed) {
                            e.preventDefault();
                            self.navigate(clean(location));
                        }
                    });
                    link.hasListenerAttached = true;
                }
            });
        },
        generate: function generate(name) {
            var data = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

            var result = this._routes.reduce(function(result, route) {
                var key;

                if (route.name === name) {
                    result = route.route;
                    for (key in data) {
                        result = result.replace(':' + key, data[key]);
                    }
                }
                return result;
            }, '');

            return this._useHash ? this._hash + result : result;
        },
        link: function link(path) {
            return this._getRoot() + path;
        },
        pause: function pause() {
            var status = arguments.length <= 0 || arguments[0] === undefined ? true : arguments[0];

            this._paused = status;
        },
        resume: function resume() {
            this.pause(false);
        },
        disableIfAPINotAvailable: function disableIfAPINotAvailable() {
            if (!isPushStateAvailable()) {
                this.destroy();
            }
        },
        lastRouteResolved: function lastRouteResolved() {
            return this._lastRouteResolved;
        },

        _add: function _add(route) {
            var handler = arguments[1];
            var config = arguments[2];
            var hooks = arguments[3];

            if (typeof route === 'string') {
                route = encodeURI(route);
            }
            this._routes.push({
                route: route,
                handler: handler,
                hooks: hooks,
                config: config
            });
            return this._add;
        },
        _getRoot: function _getRoot() {
            if (this.root !== null) return this.root;
            this.root = root(this._cLoc(), this._routes);
            return this.root;
        },
        _listen: function _listen() {
            var _this3 = this;

            if (this._usePushState) {
                window.addEventListener('popstate', this._onLocationChange);
            } else if (isHashChangeAPIAvailable()) {
                window.addEventListener('hashchange', this._onLocationChange);
            } else {
                (function() {
                    var cached = _this3._cLoc(),
                        current = void 0,
                        _check = void 0;

                    _check = function check() {
                        current = _this3._cLoc();
                        if (cached !== current) {
                            cached = current;
                            _this3.resolve();
                        }
                        _this3._listeningInterval = setTimeout(_check, 200);
                    };
                    _check();
                })();
            }
        },
        _cLoc: function _cLoc() {
            if (typeof window !== 'undefined') {
                if (typeof window.__ROUTER_WINDOW_LOCATION_MOCK__ !== 'undefined') {
                    return window.__ROUTER_WINDOW_LOCATION_MOCK__;
                }
                return clean(window.location.href);
            }
            return '';
        },
        _findLinks: function _findLinks() {
            return [].slice.call(document.querySelectorAll('[data-internal]'));
        },
        _onLocationChange: function _onLocationChange() {
            this.resolve();
        }
    };

    Router.PARAMETER_REGEXP = /([:*])(\w+)/g;
    Router.WILDCARD_REGEXP = /\*/g;
    Router.REPLACE_VARIABLE_REGEXP = '([^\/]+)';
    Router.REPLACE_WILDCARD = '(?:.*)';
    Router.FOLLOWED_BY_SLASH_REGEXP = '(?:\/$|$)';
    Router.MATCH_REGEXP_FLAGS = '';

    return Router;
}());