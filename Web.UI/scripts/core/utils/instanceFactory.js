/**
 * Instance Factory
 * Helps in creating component instances
 * Run Garbage collector periodically to check if instance can be garbage collected to free memory
 * and turn off any event subscriptions
 */
app.$if = (function () {
    var _componentRepository = {},
        _lastKey = 0,
        _elementsToUnbind = [],
        _holdGarbageCollection = false;
    _elementsToUnbind.lock = true;

    var baseComponent = (function () {
        var baseClass = function () { };
        baseClass.prototype.init = function () { };
        baseClass.prototype.destroy = function () { };
        baseClass.prototype.on = function (event, callback) {
            app.$ps.on(event, callback, this);
            this.subscriptions = (this.subscriptions || []);
            this.subscriptions.push(event);
        };
        baseClass.prototype.off = function (event) {
            app.$ps.off(event, this);
        };
        baseClass.prototype.publish = function (event, params) {
            app.$ps.publish(event, params);
        };
        baseClass.prototype.completeOff = function () {
            if (this.subscriptions) {
                while (this.subscriptions.length) {
                    this.off(this.subscriptions.pop(), this);
                }
            }
        };
        return baseClass;
    }());

    var $gc = function () {
        var interval = setInterval(function () {
            if (!_holdGarbageCollection && _elementsToUnbind.length && !_elementsToUnbind.lock) {
                removeObjects();
            }
        }, 2000);
        function removeObjects() {
            while (_elementsToUnbind.length) {
                var key = _elementsToUnbind.pop();
                var obj = _componentRepository[key];
                if (obj) {
                    obj.completeOff();
                    obj.destroy();
                    if (obj.events) {
                        for (var i = 0; i < obj.events.length; i++) {
                            var selectedElements = element.querySelectorAll(obj.events[i].selector),
                                selectedElementsEvents = obj.events[i].events.split(',');

                            selectedElements.forEach(function (selectedElement) {
                                for (var j = 0; j < selectedElementsEvents.length; j++) {
                                    selectedElement.removeEventListener(selectedElementsEvents[j], obj.events[i].handler);
                                }
                            });
                        }
                    }
                    delete _componentRepository[key];
                }
            }
            _elementsToUnbind.lock = true;
        }
    };

    return {
        init: function () {
            var self = this;
            document.querySelectorAll("[data-component]").forEach(function (element) {
                self.createCInstance(element);
            });

            $(document).bind("DOMNodeRemoved", function (e) {
                var instanceKey = e.target.dataset.instance;

                //pause gc if list in being populated
                if (_elementsToUnbind.lock) {
                    setTimeout(function () {
                        _elementsToUnbind.lock = false;
                    }, 200);
                }
                if (instanceKey) {
                    _elementsToUnbind.lock = true;
                    _elementsToUnbind.push(instanceKey);
                    var childComponents = $(e.target).find("[data-component]");
                    if (childComponents.length) {
                        $.each(childComponents, function (i, childComponent) {
                            var childInstanceKey = childComponent.dataset.instance;
                            _elementsToUnbind.push(childInstanceKey);
                        });
                    }
                }
            });
            $gc();
        },
        getInstance: function (key) {
            return _componentRepository[key];
        },
        createCInstance: function (element) {
            var componentClass = app.components[element.getAttribute("data-component")];
            if (componentClass) {
                componentClass.prototype = Object.create(baseComponent.prototype);
                componentClass.prototype.constructor = componentClass;
                var obj = new componentClass();

                var instanceKey = "key_" + (_lastKey++);
                obj.key = instanceKey;
                _componentRepository[instanceKey] = obj;
                element.setAttribute("data-instance", instanceKey);
                obj.init(element);

                if (obj.events) {
                    for (var i = 0; i < obj.events.length; i++) {
                        var selectedElements = element.querySelectorAll(obj.events[i].selector),
                            selectedElementsEvents = obj.events[i].events.split(',');
                        selectedElements.forEach(function (selectedElement) {
                            for (var j = 0; j < selectedElementsEvents.length; j++) {
                                selectedElement.addEventListener(selectedElementsEvents[j], obj.events[i].handler);
                            }
                        });
                    }
                }
            }
            else {
                app.utils.logger.logError(componentClass + "  can not be instantiated");
            }
        },
        createCInstance2: function ($html) {
            var self = this;
            var $element = $html.first();
            var parentComponent = $element.data("component");
            if (parentComponent) {
                self.createCInstance($element.get(0));
            }
            $element.find("[data-component]").each(function (i, element) {
                self.createCInstance(element);
            });
        },
        muteGC: function (mute) {
            _holdGarbageCollection = mute;
        }
    };
}());