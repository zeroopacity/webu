/**
 * $ps is messaging module between different components
 */
app.$ps = (function () {
    var mediator = {};
    var system = {
        on: function (event, callback, obj) {
            if (!mediator[event]) {
                mediator[event] = {
                    subscribers: []
                };
            }
            mediator[event].subscribers.push({
                callback: callback,
                obj: obj
            });
        },
        off: function (event, obj) {
            var subscribers = mediator[event].subscribers;
            if (subscribers) {
                var index;
                for (var i = 0; i < subscribers.length; i++) {
                    if (subscribers[i].obj.key === obj.key) {
                        index = i;
                        break;
                    }
                }
                //remove listener from subscribers list
                if (index >= 0) {
                    subscribers.splice(index, 1);
                }

                //delete group if no subscriber is present
                if (!subscribers.length) {
                    delete mediator[event];
                }
            }
        },
        publish: function (event, params) {
            var event = mediator[event];
            if (event) {
                var listeners = event.subscribers;
                for (var i = 0; i < listeners.length; i++) {
                    listeners[i].callback.call(listeners[i].obj, params);
                }
            }
        },
    };
    return system;
}());