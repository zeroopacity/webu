this["webu"] = this["webu"] || {};

this["webu"]["CreatePostTemplate"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<form method=\"post\" enctype=\"multipart/form-data\" action=\"/Home/CreatePost\">\r\n    <label>Upload Img</label>\r\n    <input name=\"PostImage\" type=\"file\" />\r\n    <button type=\"submit\">Save</button>\r\n</form>\r\n\r\n<div class=\"dropdown\">\r\n    <button class=\"dropdown-btn\">Dropdown</button>\r\n    <div class=\"dropdown-content\">\r\n        <a href=\"#\">Link 1</a>\r\n        <a href=\"#\">Link 2</a>\r\n        <a href=\"#\">Link 3</a>\r\n    </div>\r\n</div>";
},"useData":true});

this["webu"]["LoginTemplate"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<form action=\"/account/login\" method=\"post\">\r\n    UserName<br>\r\n    <input name=\"UserName\" type=\"text\" />\r\n    <button type=\"submit\">Login</button>\r\n</form>";
},"useData":true});

this["webu"]["post-template-1"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=container.escapeExpression;

  return "<div class=\"post\" data-component=\"post\">\r\n    <div class=\"post_header\">\r\n        "
    + alias1(((helper = (helper = helpers.From || (depth0 != null ? depth0.From : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"From","hash":{},"data":data}) : helper)))
    + "\r\n    </div>\r\n    <div class=\"post_content\">\r\n        "
    + alias1(container.lambda(((stack1 = (depth0 != null ? depth0.Content : depth0)) != null ? stack1.Data : stack1), depth0))
    + "\r\n    </div>\r\n    <div class=\"post_footer\">\r\n    </div>\r\n</div>";
},"useData":true});

this["webu"]["WebSocketComponent"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<h1>WebSocket Sample Application</h1>\r\n<p id=\"stateLabel\">Ready to connect...</p>\r\n<div>\r\n    <label for=\"connectionUrl\">WebSocket Server URL:</label>\r\n    <input id=\"connectionUrl\" />\r\n    <button id=\"connectButton\" type=\"submit\">Connect</button>\r\n</div>\r\n<p></p>\r\n<div>\r\n    <label for=\"sendMessage\">Message to send:</label>\r\n    <input id=\"sendMessage\" disabled />\r\n    <button id=\"sendButton\" type=\"submit\" disabled>Send</button>\r\n    <button id=\"closeButton\" disabled>Close Socket</button>\r\n</div>\r\n\r\n<h2>Communication Log</h2>\r\n<table style=\"width: 800px\">\r\n    <thead>\r\n        <tr>\r\n            <td style=\"width: 100px\">From</td>\r\n            <td style=\"width: 100px\">To</td>\r\n            <td>Data</td>\r\n        </tr>\r\n    </thead>\r\n    <tbody id=\"commsLog\">\r\n    </tbody>\r\n</table>";
},"useData":true});

this["webu"]["SearchTemplate"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<form action=\"/search\" method=\"POST\">\r\n    <label>Search Keywords</label>\r\n    <input type=\"text\" class=\"search-input\" />\r\n    <input type=\"button\" class=\"search-submit\" value=\"search\"/>\r\n</form>";
},"useData":true});

this["webu"]["userWallPost"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "";
},"useData":true});