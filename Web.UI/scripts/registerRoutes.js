/**
 * function registers the routes
 */
(function (router, components) {
    // configuration
    router.globalHooks = {
        before: function (param, done) {
            console.log("before");
            if(param.route.config.authenticate){
                alert("Url require authentication");
            }
            done(true);
        },
        after: function (params) {
            console.log("after");
        }
    };
    router.on({
        '/profile': { controller: components.userProfile, config: {authenticate:true} },
        //"/images": components.userGallery,
        //"/friends": components.userFriends,
        "/user/:userId/": { controller: components.userPostWall, config: {} },
        "/user/:userId/profile": { controller: components.userProfile, config: {} },
        //"/user/:userId/images": components.userGallery,
        //"/user/:userId/friends": components.userFriends,
        //"/about/": components.about,
        "*": { controller: components.userPostWall, config: {} }
    }).resolve();

    router.navigate('/');

} (App.utils.router, App.components));