/**
 * Scripts/app.js file
 */
var app = (function () {
    var self = {
        components: {},
        utils: {
            async: function (func) {
                setTimeout(func, 0);
            },
            logger: function (str) {
                console.log(str);
            }
        }
    };
    return self;
}());

$(document).ready(function () {
    app.$if.init();
});